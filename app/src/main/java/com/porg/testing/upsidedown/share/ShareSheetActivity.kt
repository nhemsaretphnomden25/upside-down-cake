package com.porg.testing.upsidedown.share

import android.app.PendingIntent
import android.content.Intent
import android.graphics.drawable.Icon
import android.os.Bundle
import android.os.Parcelable
import android.service.chooser.ChooserAction
import androidx.appcompat.app.AppCompatActivity
import com.porg.testing.upsidedown.R

class ShareSheetActivity : AppCompatActivity() {

    lateinit var card: SocialCard

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_share_sheet)

        card = findViewById(R.id.share_post)
        card.setShareOnClickListener {showShareSheet()}
    }

    private fun createRepostAction(): ChooserAction {
        // Create a PendingIntent for the action
        val repostIntent = Intent(this, ShareRepostActivity::class.java).apply {
            flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            putExtra(Intent.EXTRA_TEXT, getString(R.string.share_social_post))
            putExtra(Intent.EXTRA_REFERRER, getString(R.string.share_social_username))
        }
        val repostPendingIntent = PendingIntent.getActivity(
            this, 0, repostIntent,
            PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_IMMUTABLE
        )
        // Make the ChooserAction
        return ChooserAction.Builder(
            Icon.createWithResource(this, R.drawable.ic_repost),
            getString(R.string.share_btn_repost),
            repostPendingIntent
        ).build()
    }

    private fun showShareSheet() {
        // Make an array of actions, in our case there is only one
        val repostAction = createRepostAction()
        val actions: Array<Parcelable> = arrayOf(repostAction)
        // Make the intent we want a chooser for
        val sendIntent: Intent = Intent().apply {
            action = Intent.ACTION_SEND
            putExtra(Intent.EXTRA_TEXT, getString(R.string.share_social_post))
            type = "text/plain"
        }
        // Make the chooser intent and add our actions there
        val shareIntent = Intent.createChooser(sendIntent, null)
        shareIntent.putExtra(Intent.EXTRA_CHOOSER_CUSTOM_ACTIONS, actions)
        // Finally, open the chooser
        startActivity(shareIntent)
    }
}