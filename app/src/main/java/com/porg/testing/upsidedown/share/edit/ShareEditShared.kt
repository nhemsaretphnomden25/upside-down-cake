package com.porg.testing.upsidedown.share.edit

import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.net.Uri

class ShareEditShared {
    companion object {
        private fun makeEditIntent(context: Context, url: String): PendingIntent {
            val editIntent = Intent(context, ShareEditAccessActivity::class.java).apply {
                flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                data = Uri.parse(url)
            }
            return PendingIntent.getActivity(
                context, 0, editIntent,
                PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_IMMUTABLE
            )
        }

        fun showShareSheet(context: Context) {
            val url = "https://doc.example/view/hdvn9KwdRU28dgPicnGlNg"
            // Make the intent we want a chooser for
            val sendIntent: Intent = Intent().apply {
                // note: normally you would use ACTION_VIEW for URLs - here ACTION_SEND
                // is used to make sure a chooser appears as the API is related to the
                // share sheet
                action = Intent.ACTION_SEND
                putExtra(Intent.EXTRA_TEXT, url)
                type = "text/plain"
            }
            // Make the chooser intent and add our edit action
            val shareIntent = Intent.createChooser(sendIntent, null)
            shareIntent.putExtra(Intent.EXTRA_CHOOSER_MODIFY_SHARE_ACTION, makeEditIntent(context, url))
            // Finally, open the chooser
            context.startActivity(shareIntent)
        }
    }
}