package com.porg.testing.upsidedown

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import com.porg.testing.upsidedown.battery.BatteryStatusActivity
import com.porg.testing.upsidedown.gender.GenderActivity
import com.porg.testing.upsidedown.install.PackageInstallActivity
import com.porg.testing.upsidedown.screenshot.ScreenshotActivity
import com.porg.testing.upsidedown.share.ShareSheetActivity
import com.porg.testing.upsidedown.share.edit.ShareEditActivity

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        findViewById<Button>(R.id.main_btn_gender).setOnClickListener {
            startActivity(Intent(this, GenderActivity::class.java))
        }
        findViewById<Button>(R.id.main_btn_screenshot).setOnClickListener {
            startActivity(Intent(this, ScreenshotActivity::class.java))
        }
        findViewById<Button>(R.id.main_btn_preapp).setOnClickListener {
            startActivity(Intent(this, PackageInstallActivity::class.java))
        }
        findViewById<Button>(R.id.main_btn_share).setOnClickListener {
            startActivity(Intent(this, ShareSheetActivity::class.java))
        }
        findViewById<Button>(R.id.main_btn_shareedit).setOnClickListener {
            startActivity(Intent(this, ShareEditActivity::class.java))
        }
        findViewById<Button>(R.id.main_btn_battery).setOnClickListener {
            startActivity(Intent(this, BatteryStatusActivity::class.java))
        }
    }

}