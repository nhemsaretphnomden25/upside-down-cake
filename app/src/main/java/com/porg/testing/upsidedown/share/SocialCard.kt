package com.porg.testing.upsidedown.share

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.porg.testing.upsidedown.R

class SocialCard @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) :
    ConstraintLayout(context, attrs, defStyleAttr) {

    private lateinit var nameView: TextView
    private lateinit var usernameView: TextView
    private lateinit var postView: TextView
    private lateinit var shareButton: Button
    private lateinit var likeButton: Button

    init {
        init(attrs)
    }

    private fun init(attrs: AttributeSet?) {
        View.inflate(context, R.layout.social_card, this)

        nameView = findViewById(R.id.social_card_name)
        usernameView = findViewById(R.id.social_card_username)
        postView = findViewById(R.id.social_card_post)
        shareButton = findViewById(R.id.social_card_repost)
        likeButton = findViewById(R.id.social_card_like)

        val ta = context.obtainStyledAttributes(attrs, R.styleable.SocialCard)
        try {
            nameView.text = ta.getString(R.styleable.SocialCard_name)
            usernameView.text = ta.getString(R.styleable.SocialCard_username)
            postView.text = ta.getString(R.styleable.SocialCard_postBody)
        } finally {
            ta.recycle()
        }
    }

    fun setShareOnClickListener(listener: OnClickListener) {
        shareButton.setOnClickListener(listener)
    }
    fun setPostBody(body: String) {
        postView.text = body
    }
    fun setPostName(name: String) {
        nameView.text = name
    }
    fun setPostUsername(name: String) {
        usernameView.text = name
    }
    fun disableInteractions() {
        shareButton.isEnabled = false
        likeButton.isEnabled = false
    }
}