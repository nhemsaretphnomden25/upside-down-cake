package com.porg.testing.upsidedown.share

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.porg.testing.upsidedown.R
import java.util.Locale

class ShareRepostActivity : AppCompatActivity() {
    fun getDeviceName(): String {
        val manufacturer = Build.MANUFACTURER
        val model = Build.MODEL
        return if (model.lowercase(Locale.getDefault())
                .startsWith(manufacturer.lowercase(Locale.getDefault()))
        ) model
        else "$manufacturer $model"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_share_repost)
        // Get some data
        val post = intent.getStringExtra(Intent.EXTRA_TEXT)
        val op = intent.getStringExtra(Intent.EXTRA_REFERRER)
        val card = findViewById<SocialCard>(R.id.share_repost)
        // Make our new "post"
        val body = getString(R.string.share_social_repost_template).format(op, post)
        card.setPostName(getDeviceName())
        card.setPostUsername("@lonely_${Build.BOARD}")
        card.setPostBody(body)
        card.disableInteractions()
        // Show a warning
        Toast.makeText(this, getString(R.string.share_repost_success), Toast.LENGTH_SHORT).show()
    }
}