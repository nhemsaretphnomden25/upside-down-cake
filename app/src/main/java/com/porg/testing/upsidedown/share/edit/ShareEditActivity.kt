package com.porg.testing.upsidedown.share.edit

import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.appbar.MaterialToolbar
import com.porg.testing.upsidedown.R

class ShareEditActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_share_edit)
        // Load our "document"
        val text = findViewById<TextView>(R.id.share_edit_text)
        text.text = readDocument()
        // Add listener
        val tab = findViewById<MaterialToolbar>(R.id.share_edit_topbar)
        tab.setOnMenuItemClickListener { menuItem ->
            when (menuItem.itemId) {
                R.id.share_edit_button -> {
                    // Show the share sheet.
                    ShareEditShared.showShareSheet(this)
                    true
                }

                else -> false
            }
        }
    }

    fun readDocument(): String {
        return assets.open("document.txt").bufferedReader().use { it.readText() }
    }

}