package com.porg.testing.upsidedown.share.edit

import android.os.Bundle
import android.widget.Button
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import com.google.android.material.materialswitch.MaterialSwitch
import com.porg.testing.upsidedown.R

class ShareEditAccessActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_share_edit_access)
        // Add the listener for the "Share again" button
        val shareAgain = findViewById<Button>(R.id.share_edit_share_again)
        shareAgain.setOnClickListener { ShareEditShared.showShareSheet(this) }
        // Add a listener for the toggle
        val permLayout = findViewById<LinearLayout>(R.id.share_edit_perm_layout)
        val private = findViewById<MaterialSwitch>(R.id.share_edit_private)
        permLayout.isVisible = !private.isChecked
        private.setOnCheckedChangeListener { _, state ->
            permLayout.isVisible = !state
        }
    }
}