package com.porg.testing.upsidedown.screenshot

import android.app.Activity.ScreenCaptureCallback
import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.porg.testing.upsidedown.R

class ScreenshotActivity : AppCompatActivity() {

    var messageNum = 0
    var messages: Array<String> = arrayOf()
    var text: TextView? = null

    // Important - this gets called whenever a screenshot is taken.
    val screenCaptureCallback = ScreenCaptureCallback {
        // Update the message
        text!!.text = messages[messageNum]
        // Increment the counter
        messageNum++
        if (messageNum >= messages.size) messageNum = messages.size - 1
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_screenshot)
        // Get a few resources
        messages = resources.getStringArray(R.array.you_made_a_screenshot)
        text = findViewById(R.id.screenText)
    }

    override fun onStart() {
        super.onStart()
        // Pass in the callback created in the previous step and the intended callback executor.
        registerScreenCaptureCallback(mainExecutor, screenCaptureCallback)
    }

    override fun onStop() {
        super.onStop()
        // Unregister the callback as it's no longer necessary.
        unregisterScreenCaptureCallback(screenCaptureCallback)
    }
}