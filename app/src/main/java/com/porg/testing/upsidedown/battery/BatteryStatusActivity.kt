package com.porg.testing.upsidedown.battery

import android.content.Intent
import android.content.IntentFilter
import android.os.BatteryManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import android.widget.Toast
import com.porg.testing.upsidedown.R

class BatteryStatusActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_battery_status)

        // Receive the current battery status
        val possibleBatteryStatus: Intent? = IntentFilter(Intent.ACTION_BATTERY_CHANGED).let { ifilter ->
            this.registerReceiver(null, ifilter)
        }
        if (possibleBatteryStatus == null) {
            Toast.makeText(this, "Cannot get battery data", Toast.LENGTH_SHORT).show()
            finish()
        }
        // This is to get lint to shut up,
        // it's pretty much guaranteed to not be null as the activity finishes if it is
        val batteryStatus = possibleBatteryStatus!!

        // Show cycle count
        var cycles: String = batteryStatus.getIntExtra("android.os.extra.CYCLE_COUNT", -1).toString()
        if (cycles == "-1") cycles = "???"
        findViewById<TextView>(R.id.battery_cycles).text = cycles
        // Get percentage
        val level: Int = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1)
        val scale: Int = batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1)
        val percentage: Int = ((level.toFloat()/scale)*100).toInt()
        findViewById<TextView>(R.id.battery_percentage).text = "${percentage}%"
        // Get charging status
        val chargingStatus: Int = batteryStatus.getIntExtra("android.os.extra.CHARGING_STATUS", -1)
        findViewById<TextView>(R.id.battery_charge_status).text = chargingStatus.toString()
    }
}