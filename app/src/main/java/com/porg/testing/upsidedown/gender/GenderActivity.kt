package com.porg.testing.upsidedown.gender

import android.app.GrammaticalInflectionManager
import android.content.Context
import android.content.res.Configuration
import android.os.Bundle
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayout.OnTabSelectedListener
import com.porg.testing.upsidedown.R

class GenderActivity : AppCompatActivity() {

    var currentGender: Int = Configuration.GRAMMATICAL_GENDER_NOT_SPECIFIED
    var inflectionManager: GrammaticalInflectionManager? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_gender)
        // request the inflection manager
        inflectionManager = getSystemService(Context.GRAMMATICAL_INFLECTION_SERVICE) as GrammaticalInflectionManager
        // find the tab layout
        val tabLayout = findViewById<TabLayout>(R.id.gender_tabs)
        tabLayout.addOnTabSelectedListener(object : OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                // ask the OS to use the selected gender
                inflectionManager!!.setRequestedApplicationGrammaticalGender(when (tab.position) {
                    0 -> Configuration.GRAMMATICAL_GENDER_FEMININE
                    1 -> Configuration.GRAMMATICAL_GENDER_MASCULINE
                    2 -> Configuration.GRAMMATICAL_GENDER_NEUTRAL
                    else -> Configuration.GRAMMATICAL_GENDER_NOT_SPECIFIED
                })
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {}
            override fun onTabReselected(tab: TabLayout.Tab) {}
        })
    }

    fun inflect(gender: Int = 0) {
        // if no gender was given, request it from the OS
        var _gender = gender
        if (_gender == 0)
            _gender = inflectionManager!!.applicationGrammaticalGender
        // set the string based on the gender
        val string = getString(when (_gender) {
            Configuration.GRAMMATICAL_GENDER_NEUTRAL -> R.string.status_busy_n
            Configuration.GRAMMATICAL_GENDER_FEMININE -> R.string.status_busy_f
            Configuration.GRAMMATICAL_GENDER_MASCULINE -> R.string.status_busy_m
            else -> R.string.status_busy
        })
        findViewById<TextView>(R.id.gender_auto_label).text = string
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        // show the new gender
        Toast.makeText(this, when (newConfig.grammaticalGender) {
            Configuration.GRAMMATICAL_GENDER_NEUTRAL -> "Gender is neutral"
            Configuration.GRAMMATICAL_GENDER_FEMININE -> "Gender is feminine"
            Configuration.GRAMMATICAL_GENDER_MASCULINE -> "Gender is masculine"
            Configuration.GRAMMATICAL_GENDER_NOT_SPECIFIED -> "Gender is unset"
            else -> "Gender is something else"
        }, Toast.LENGTH_SHORT).show()
        // inflect the string
        inflect(newConfig.grammaticalGender)
    }

}