package com.porg.testing.upsidedown.share

import android.app.PendingIntent
import android.content.Intent
import android.graphics.drawable.Icon
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Parcelable
import android.service.chooser.ChooserAction
import com.porg.testing.upsidedown.R

class ShareActionSpamActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_share_action_spam)
        showShareSheet()
    }


    private fun createCountingAction(counter: Int): ChooserAction {
        // Create a PendingIntent for the action
        val repostIntent = Intent(this, ShareActionSpamActivity::class.java).apply {
            flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            putExtra(Intent.EXTRA_TEXT, getString(R.string.share_social_post))
        }
        val repostPendingIntent = PendingIntent.getActivity(
            this, 0, repostIntent,
            PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_IMMUTABLE
        )
        // Make the ChooserAction
        return ChooserAction.Builder(
            Icon.createWithResource(this, R.drawable.ic_gender_neutral),
            "a",//counter.toString(),
            repostPendingIntent
        ).build()
    }

    private fun showShareSheet() {
        //val actions: Array<Parcelable> = arrayOf(repostAction)
        val actions: MutableList<Parcelable> = mutableListOf()
        for (i in 1..2545) {
            actions.add(createCountingAction(i))
        }
        // Make the intent we want a chooser for
        val sendIntent: Intent = Intent().apply {
            action = Intent.ACTION_SEND
            putExtra(Intent.EXTRA_TEXT, "Is this enough actions? (follow @narek@fosstodon.org btw)")
            type = "text/plain"
        }
        // Make the chooser intent and add our actions there
        val shareIntent = Intent.createChooser(sendIntent, null)
        shareIntent.putExtra(Intent.EXTRA_CHOOSER_CUSTOM_ACTIONS, actions.toTypedArray())
        // Finally, open the chooser
        startActivity(shareIntent)
    }
}