# Android U Testing

Testing/showcasing various new APIs introduced in Android 14, aka Upside Down Cake.

## Building

To build this repo you need to use the **latest canary** release of Android Studio (2022.3.1 Giraffe preview 8 at the time of writing), available [here](https://developer.android.com/studio/preview). You will also need the latest (DP2) version of the Android API 34 platform, and an emulator image if you don't have a device to test it on.

## APIs included

### New in DP1
- Grammatical inflection/gender

### New in DP2
- Screenshot detection (untested)
- Install pre-approval (WIP)
